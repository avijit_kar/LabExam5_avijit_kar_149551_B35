
<?php
class MyCalculator {
    public $_num1, $_num2;


    public function __construct( $_num1, $_num2 ) {
        $this->_num1 = $_num1;
        $this->_num2 = $_num2;
    }

    public function add() {
        return $this->_num1 + $this->_num2;
    }


    public function subtract() {
        return $this->_num1 - $this->_num2;
    }

    public function multiply() {
        return $this->_num1 * $this->_num2;
    }
public function divide()
{
    return $this->_num1 / $this->_num2;
}

}
$mycalc= new MyCalculator(12,6);

echo "Add :".$mycalc->add();
echo "<br>";
echo "Multiply :".$mycalc->multiply();
echo "<br>";
echo "Subtract :".$mycalc->subtract();
echo "<br>";
echo "Divide :". $mycalc->divide();
?>

