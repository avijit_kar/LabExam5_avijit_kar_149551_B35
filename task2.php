<?php

$startdate = new DateTime("1981-11-03");

$enddate = new DateTime("2013-09-04");

$interval = $enddate->diff($startdate);

echo "Difference : " . $interval->y. " years, " . $interval->m." months, ".$interval->d." days ";

?>